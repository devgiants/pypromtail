""" Handle log files to differents log handlers. """
__author__ = "Overflaw"
__version__ = "0.5.0"
__license__ = "MIT"

import os.path
import sys
import yaml
import json
import argparse
import logzero
from logzero import logger

loggingLevel = logzero.INFO
logzero.loglevel(loggingLevel)
logzero.logfile("./logs/pypromtail.log", maxBytes=1e6, backupCount=50, loglevel=loggingLevel)

from cerberus import Validator

from log_ingest_context.adapters.Secondary.last_line_marker import LastLineMarker
from log_ingest_context.adapters.Secondary.loki_log_repository import LokiLogRepository
from log_ingest_context.business_logic.use_cases.log_ingest.log_ingest_command_handler import LogIngestCommandHandler
from log_ingest_context.business_logic.use_cases.log_ingest.log_ingest_command import LogIngestCommand

if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-f",
        "--file",
        default=False,
        required=True,
        help="Config file path"
    )

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity (-v, -vv, etc)")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()

    with open(args.file, "r") as stream:
        config_scheme_file_object = open(r"./configuration_schema.json")
        config_scheme = json.load(config_scheme_file_object)
        validator = Validator()
        config = yaml.safe_load(stream)

        if (not validator.validate(config, config_scheme)):
            logger.error('YAML configuration errors : %s', validator.errors)
            sys.exit()

        config = config["pypromtail"]
        log_repository = LokiLogRepository(config)        

        ingester_handle = LogIngestCommandHandler(log_repository)
        for logfile_data in config['log_sources']:
            marker = LastLineMarker(logfile_data['path'])
            try:                
                ingester_handle.handle(
                    LogIngestCommand(
                        logfile_data['path'],
                        logfile_data['regex'],
                        logfile_data['mapping'],
                        logfile_data['source']
                    ),
                    marker
                )
            except Exception as e:
                logger.error(e)
            finally:
                last_raw_line = log_repository.flush()
                logger.info(f"Mark file {logfile_data['path']} with {last_raw_line}")
                marker.mark(last_raw_line)

