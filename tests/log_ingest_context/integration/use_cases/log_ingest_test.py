import os
import pytest
from tests.log_ingest_context.unit.doubles.fake_log_repository import FakeLogRepository


def test_complete_log_handling():
    last_line_file_path = r"./tests/data/example_complete.log.lastline"
    if os.path.exists(last_line_file_path):
        os.remove(last_line_file_path)

    
    with open(r"./tests/data/example_complete.log") as f:
        for line in f:
            pass
        last_log_line = line

    os.system(r" python pypromtail.py --file=configuration.yaml")
    assert (os.path.exists(last_line_file_path))
    with open(last_line_file_path, 'r') as f:
        assert f.readline() == last_log_line
        os.remove(last_line_file_path)

