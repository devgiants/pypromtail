from log_ingest_context.business_logic.use_cases.log_ingest.log_ingest_command_handler import LogIngestCommandHandler
from log_ingest_context.business_logic.use_cases.log_ingest.log_ingest_command import LogIngestCommand
from log_ingest_context.business_logic.exceptions.line_no_match_pattern_exception import LineNoMatchPatternException
from log_ingest_context.business_logic.exceptions.missing_mandatory_group_exception import \
    MissingMandatoryGroupException
from tests.log_ingest_context.unit.doubles.fake_log_repository import FakeLogRepository
from log_ingest_context.adapters.Secondary.last_line_marker import LastLineMarker
from cerberus import Validator
import pytest
import yaml
import json

@pytest.fixture
def setup():
    return {
        "log_repository": FakeLogRepository()        
    }


def test_complete_log_handling(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )
    ingester_handler.handle(
        LogIngestCommand(
            r"./tests/data/example_complete.log",
            r"\[(?P<severity>\w)\s+(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\s+(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})\s+(?P<method_name>\w*):(?P<line_number>\d{2})\]\s+(?P<message>.*)",
            {
                'severity': {
                    'D': 'debug',
                    'I': 'information',
                    'W': 'warning',
                    'E': 'error'
                }
            },
            'MDS'
        ),
        LastLineMarker(r"./tests/data/example_complete.log")
    )
    assert (len(setup["log_repository"].logs) == 100)


def test_no_match_log_handling(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )

    with pytest.raises(LineNoMatchPatternException):
        ingester_handler.handle(
            LogIngestCommand(
                r"./tests/data/simple_log.txt",
                r"\[(?P<severity>\w)\s+(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\s+(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})\s+(?P<method_name>\w*):(?P<line_number>\d{2})\]\s+(?P<message>.*)",
                {},
                'MDS'
            ),
            LastLineMarker(r"./tests/data/simple_log.txt")
        )


def test_incomplete_log_handling(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )
    with pytest.raises(MissingMandatoryGroupException):
        ingester_handler.handle(
            LogIngestCommand(
                r"./tests/data/example_incomplete.log",
                r"\[(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\s+(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})\s+(?P<method_name>\w*):(?P<line_number>\d{2})\]\s+(?P<message>.*)",
                {},
                'MDS'
            ),
            LastLineMarker(r"./tests/data/example_incomplete.log")
        )


def test_non_existing_log_file(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )
    with pytest.raises(FileNotFoundError):
        ingester_handler.handle(
            LogIngestCommand(
                r"./tests/data/log_does_not_exists.txt",
                "",
                {},
                'MDS'
            ),
            LastLineMarker(r"./tests/data/log_does_not_exists.txt")
        )


def test_configuration():
    with open(r"./tests/data/configuration.yaml", "r") as stream:
        config_scheme_file_object = open(r"./tests/data/configuration_schema.json")
        config_scheme = json.load(config_scheme_file_object)
        validator = Validator()

        assert (validator.validate(yaml.safe_load(stream), config_scheme) == True)


def test_complete_log_handling_with_marker(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )
    file_marker = LastLineMarker(r"./tests/data/example_complete_with_marker.log")
    ingester_handler.handle(
        LogIngestCommand(
            r"./tests/data/example_complete_with_marker.log",
            r"\[(?P<severity>\w)\s+(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\s+(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})\s+(?P<method_name>\w*):(?P<line_number>\d{2})\]\s+(?P<message>.*)",
            {
                'severity': {
                    'D': 'debug',
                    'I': 'information',
                    'W': 'warning',
                    'E': 'error'
                }
            },
            'MDS'
        ),
        file_marker
    )
    assert (len(setup["log_repository"].logs) == 72)


def test_complete_log_handling_with_marker_not_related(setup):
    ingester_handler = LogIngestCommandHandler(
        setup["log_repository"]
    )
    file_marker = LastLineMarker(r"./tests/data/example_complete_with_marker_not_related.log");
    ingester_handler.handle(
        LogIngestCommand(
            r"./tests/data/example_complete_with_marker_not_related.log",
            r"\[(?P<severity>\w)\s+(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\s+(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})\s+(?P<method_name>\w*):(?P<line_number>\d{2})\]\s+(?P<message>.*)",
            {
                'severity': {
                    'D': 'debug',
                    'I': 'information',
                    'W': 'warning',
                    'E': 'error'
                }
            },
            'MDS'
        ),
        file_marker
    )
    assert (len(setup["log_repository"].logs) == 100)