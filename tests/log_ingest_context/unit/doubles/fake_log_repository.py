from log_ingest_context.business_logic.gateways.log_repository_interface import LogRepositoryInterface
from log_ingest_context.business_logic.models.log_entry import LogEntry


class FakeLogRepository(LogRepositoryInterface):
    def __init__(self):
        self.logs = []

    def save(self, log_entry: LogEntry):
        self.logs.append(log_entry)
