import os.path
from log_ingest_context.business_logic.gateways.last_line_marker_interface import LastLineMarkerInterface


class LastLineMarker(LastLineMarkerInterface):
    def is_marked(self) -> bool:
        if not os.path.exists(self.get_marker_filename()):
            return False
        with open(self.get_marker_filename(), 'r') as marked_file:
            last_line = marked_file.readline()
            with open(self.file_path, 'r') as file:
                lines = file.readlines()
                for line in lines:
                    self.last_line_position += 1
                    if line == last_line:
                        return True
                self.last_line_position = 0
        return False

    def mark(self, last_raw_line: str) -> None:
        with open(self.get_marker_filename(), 'w') as file:
            file.write(last_raw_line)

    def is_last_line(self, line: str) -> bool:
        if not self.is_marked():
            raise Exception("File is not marked")
        with open(self.get_marker_filename(), 'r') as file:
            return line == file.readline()

    def get_marker_filename(self) -> str:
        return self.file_path + ".lastline"
