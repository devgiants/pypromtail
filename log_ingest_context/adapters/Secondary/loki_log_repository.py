import re

import requests
import json
import time
from datetime import datetime
from logzero import logger
from log_ingest_context.business_logic.gateways.log_repository_interface import LogRepositoryInterface
from log_ingest_context.business_logic.models.log_entry import LogEntry


class LokiLogRepository(LogRepositoryInterface):
    HEADERS = {
        'Content-type': 'application/json'
    }

    def __init__(self, config: dict):
        self.url = config['log_destination']['url']
        self.host = config['host']
        self.logs = []

    def save(self, log_entry: LogEntry):
        self.logs.append(log_entry)

    def flush(self):
        logger.info("Start flushing %d log entries", len(self.logs))
        last_raw_line = self.__sendLogEntries()
        logger.info("Clear log entries")
        self.logs.clear()
        return last_raw_line

    def __sendLogEntries(self):
        payload = {"streams": []}

        last_raw_line = ""
        for log_entry in self.logs:
            stream = {
                'stream': {
                    'source': self.host,
                    'job': log_entry.meta["job"],
                    'host': self.host,
                    'level': log_entry.severity,
                    'log_date': datetime.fromtimestamp(log_entry.timestamp).strftime('%Y-%m-%d'),
                    'log_time': datetime.fromtimestamp(log_entry.timestamp).strftime('%H-%M-%S'),
                },
                'values': [
                    [
                        "{}000000000".format(int(time.time())),
                        log_entry.message
                    ]
                ]
            }
            last_raw_line = log_entry.raw_line
            payload["streams"].append(stream)

        logger.debug(payload)
        response = requests.post(self.url, data=json.dumps(payload), headers=self.HEADERS)        
        logger.debug("code %d : %s", response.status_code, response.content)
        return last_raw_line
        
