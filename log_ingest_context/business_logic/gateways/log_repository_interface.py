from log_ingest_context.business_logic.models.log_entry import LogEntry


class LogRepositoryInterface:
    def save(self, log_entry: LogEntry):
        raise Exception("Must be implemented")

    def flush(self):
        raise Exception("Must be implemented")