

class LastLineMarkerInterface:

    def __init__(self, file_path: str):
        self.file_path = file_path
        self.last_line_position = 0

    def is_marked(self) -> bool:
        raise Exception("Must be implemented")

    def mark(self, last_raw_line: str) -> None:
        raise Exception("Must be implemented")

    def is_last_line(self, line: str) -> bool:
        raise Exception("Must be implemented")