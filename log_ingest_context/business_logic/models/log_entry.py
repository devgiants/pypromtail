class LogEntry:
    def __init__(self, raw_line: str, timestamp: int, severity: str, message: str, meta: dict):
        self.raw_line = raw_line
        self.timestamp = timestamp
        self.severity = severity
        self.message = message
        self.meta = meta
