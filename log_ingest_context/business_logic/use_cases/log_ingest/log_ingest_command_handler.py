import os
import re
from datetime import datetime
from logzero import logger
from itertools import islice
from log_ingest_context.business_logic.use_cases.log_ingest.log_ingest_command import LogIngestCommand
from log_ingest_context.business_logic.gateways.log_repository_interface import LogRepositoryInterface
from log_ingest_context.adapters.Secondary.last_line_marker import LastLineMarker
from log_ingest_context.business_logic.exceptions.line_no_match_pattern_exception import LineNoMatchPatternException
from log_ingest_context.business_logic.exceptions.missing_mandatory_group_exception import \
    MissingMandatoryGroupException
from log_ingest_context.business_logic.models.log_entry import LogEntry


class LogIngestCommandHandler:
    MANDATORY_KEYS = [
        'severity',
        'year',
        'month',
        'day',
        'hour',
        'minute',
        'second',
        'message'
    ]

    def __init__(self, log_repository: LogRepositoryInterface):
        self.groups = None
        self.log_ingest_command = None
        self.log_repository = log_repository

    def handle(self, log_ingest_command: LogIngestCommand, file_marker: LastLineMarker):
        self.log_ingest_command = log_ingest_command
        if not os.path.exists(self.log_ingest_command.logFile):
            logger.error("File %s does not exist", self.log_ingest_command.logFile)
        with open(log_ingest_command.logFile, 'r') as file:
            # lines = file.readlines()
            # logger.info("%d lines to handle", len(lines))
            current_line_count = 1
            start_line = 0
            if file_marker.is_marked():
                start_line = file_marker.last_line_position
            else:
                logger.info("File %s is not marked as started", log_ingest_command.logFile)

            for line in islice(file, start_line, None):
                match = re.match(
                    log_ingest_command.regexPattern,
                    line)
                if match:
                    self.groups = match.groupdict()
                    self.__check_mandatory_groups(self.groups)
                    year = self.__extract_group_value('year')
                    if len(year) == 2:
                        year = int(year) + 2000
                    log_date_time = datetime(
                        int(year),
                        int(self.__extract_group_value('month')),
                        int(self.__extract_group_value('day')),
                        int(self.__extract_group_value('hour')),
                        int(self.__extract_group_value('minute')),
                        int(self.__extract_group_value('second'))
                    )
                    timestamp = int(log_date_time.timestamp())
                    self.groups["job"] = log_ingest_command.job
                    self.log_repository.save(
                        LogEntry(
                            line,
                            timestamp,
                            self.__extract_group_value('severity'),
                            self.groups.pop('message'),
                            self.groups,
                        )
                    )
                    current_line_count += 1
                else:
                    raise LineNoMatchPatternException(f"Line {current_line_count} don't match regex")

    def __check_mandatory_groups(self, groups):
        for mandatory_key in self.MANDATORY_KEYS:
            if mandatory_key not in groups:
                raise MissingMandatoryGroupException(f"Group {mandatory_key} is not found")

    def __extract_group_value(self, key: str) -> str:
        value = self.groups.pop(key)
        if key in self.log_ingest_command.mapping:
            return self.log_ingest_command.mapping[key][value]
        return value
