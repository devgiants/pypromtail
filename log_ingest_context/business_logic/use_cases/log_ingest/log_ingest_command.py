class LogIngestCommand:
    def __init__(self, logFile: str, regexPattern: str, mapping: dict, job: str):
        self.logFile = logFile
        self.regexPattern = regexPattern
        self.mapping = mapping
        self.job = job
