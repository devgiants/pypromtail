# pyPromtail

PyPromtail aims to bring most of Promtail promise (a bit pretentious though) on any Python3-ready system, without having to install anything.
It thus allows send to any Loki instances log files of your choice, even if you're running your app on mutualized server,
or regaular server but without any root permissions.