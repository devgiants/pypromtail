tdd:
	ptw

up:
	docker compose up -d

down:
	docker compose down

run: up
	python pypromtail.py --file=configuration.yaml